package floorball;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.ParameterType;
import io.cucumber.java.en.*;

public class JpetStore {
	
	
	WebDriver driver;	
	
	
	
	
	@Given("un navigateur est ouvert")
	public void un_navigateur_est_ouvert() {
		System.setProperty("webdriver.chrome.driver", "./rsc/chromedriver.exe");
		driver = new ChromeDriver();
	    driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		
	    // Write code here that turns the phrase above into concrete actions
	  
	}

	@When("Je suis sur url")
	public void je_suis_sur_url() {
		 driver.get("https://petstore.octoperf.com/actions/Catalog.action");
		
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("je clique sur le lien connexion")
	public void je_clique_sur_le_lien_connexion() {
		driver.findElement(By.xpath("//div[@id='MenuContent']/a[2]")).click();
	    // Write code here that turns the phrase above into concrete actions
	 
	}

	@When("entre le username {string}")
	public void entre_le_username(String username) {
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(username);
	    // Write code here that turns the phrase above into concrete actions
	    
	}

	@When("entre le Password {string}")
	public void entre_le_Passworde(String password) {
		driver.findElement(By.name("password")).clear();
		driver.findElement(By.name("password")).sendKeys(password);
	    // Write code here that turns the phrase above into concrete actions
	   
	}

	@When("je clique sur le login")
	public void je_clique_sur_le_login() {
		driver.findElement(By.name("signon")).click();
	    // Write code here that turns the phrase above into concrete actions
	
	}

	@Then("utilisateur abc est connecte")
	public void utilisateur_abc_est_connecte() {
		assertEquals(driver.findElement(By.xpath("//div[@id='MenuContent']/a[2]")).getText(), "Sign Out");
	    // Write code here that turns the phrase above into concrete actions

	}

	@Then("je peux lire le message accueil {string}")
	public void je_peux_lire_le_message_accueil_Welcome_AB(String returnMessage) {
		assertEquals(driver.findElement(By.id("WelcomeContent")).getText(), returnMessage);
		driver.close();
		
	    // Write code here that turns the phrase above into concrete actions
	    
	}
	

}
