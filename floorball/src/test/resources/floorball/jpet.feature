#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template

Feature: Connexion a application JpetStore
  I want to use this template for my feature file


  Scenario Outline: Connexion
    Given un navigateur est ouvert
    When Je suis sur url
    And je clique sur le lien connexion
    And entre le username <Login>
    And entre le Password <password>
    And je clique sur le login
    Then utilisateur abc est connecte
    And je peux lire le message accueil <returnMessage>
    
     Examples: 
      | Login  | password   | returnMessage  |
      | "j2ee" |     "j2ee" | "Welcome ABC!" |
      | "ACID" |     "ACID" | "Welcome ABC!"    |

  #@tag2
  #Scenario Outline: Title of your scenario outline
    #Given I want to write a step with <name>
    #When I check for the <value> in step
    #Then I verify the <status> in step
#
    #Examples: 
      #| name  | value | status  |
      #| name1 |     5 | success |
      #| name2 |     7 | Fail    |
